package com.octogonalmist.pizzarecipes;

public class PizzaRecipeItem {

    private int imaheResource;
    private String title;
    private String description;
    private String recipe;



    public PizzaRecipeItem(int imaheResource, String title, String description, String recipe) {
        this.imaheResource = imaheResource;
        this.title = title;
        this.description = description;
        this.recipe = recipe;
    }

    public int getImaheResource() {
        return imaheResource;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getRecipe() {
        return recipe;
    }
}
